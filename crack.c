#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int compare(const void *a, const void *b)
{
    return strcmp(*(char **)a, *(char **)b); //compare the strings for qsort
}

int bcompare(const void *a, const void *b) //compare only the first 32 bits of hash and compare it to the dict
{
    return strncmp((char *)a, *(char**)b,32);
}


// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    //malloc space for entire file
    //get size of file
    struct stat st; //creates a structure to find length of file
    if(stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
        
    }
    int len = st.st_size; //get length of file
    
    char *file = malloc(len); //unsigned char is raw bytes , char is characters
    //read entire file into memory
    FILE *f = fopen(filename, "r");
    
    if(!f)
    {
        printf("Can't open %s for read \n", filename);
        exit(1);
    }
    fread(file, 1, len, f); //read from file and each block is 
    fclose(f);
    //replace \n with \0
    int count = 0;
    
    for(int i =0; i<len; i++)
    {
        if(file[i] == '\n')
        {
            file[i] = '\0';
            count ++;
        }
    }
    //malloc space for array of pointers
    char **line = malloc((count+1) * sizeof(char*)); //characters to pointers
    
    //fill in addresses
    int word = 0;
    line[word] = file; //the first word in the fill
    word ++;
    for (int i = 1; i<len; i++)
    {
        if (file[i] == '\0' && i+1 < len)
        {
            line[word] = &file[i+1];
            word++;
        }
    }
    line[word] = NULL;
    //return address of second array
    return line; 
    free(line);
    
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{

    struct stat st;
    if(stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len1 = st.st_size;
    char *file1 = malloc(len1);
    FILE *g = fopen(filename, "r");
    if(!g)
    {
        printf("Can't open %s for read \n", filename);
        exit(1);
    }
    fread(file1, 1, len1, g);
    fclose(g);
    
    int count1 = 0;
    for(int i =0; i<len1; i++)
    {
        if(file1[i] == '\n')
        {
            file1[i] = '\0';
            count1++;
        }
    }
    //malloc space for array of pointers
    char **line1 = malloc((count1+1) * sizeof(char*)); //characters to pointers

    int word1 = 0;
    int bounce = 0;
    line1[word1] = file1; //the first word in the fill
    word1 ++;
    for (int i = 1; i<len1; i++)
    {
        if (file1[i] == '\0' && i+1 < len1)
        {
            //hash password malloc space for the hash plus one and
            //printf("Hashing >%s<\n", &file1[bounce]);
            char *hash1 = md5(&file1[bounce], strlen(&file1[bounce]));
            char *hash2 = malloc(strlen(hash1)+strlen(&file1[bounce])+2);
            sprintf(hash2, "%s %s" , hash1, &file1[bounce]);
            line1[word1] = hash2;
            word1++;
            bounce = i +1;
            free(hash1);
            /*char *hash1 = md5(&file1[i+1], strlen(&file1[i+1]));
            char *hash2 = malloc(strlen(hash1) + strlen(&file1[i+1]) +2);
            sprintf(hash2, "%s %s", hash1, &file1[i+1]);
            line1[word1] = hash2;
            word1++;
            free(hash1);
           */
        }
        
    }
    line1[word1] = NULL;
    //return address of second array
    return line1;
    
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    int dlength = 0;
    for (int i = 0; dict[i]; i++)
    {
        dlength++;
    }
    //sorts the dictionary
    qsort(dict, dlength, sizeof(char*), compare);
    /*for (int i = 0; dict[i]; i++)
    {
       printf("%s\n", dict[i]);
    }
    */
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int cracknum = 1;
    int count = 1;
    for (int i = 0; hashes[i]; i++)
    {
        //printf("cracking: %s\n" , hashes[i]);
        //printf("%d \n", dlength);
        cracknum++;
        char **match = (char **) bsearch(hashes[i], dict, dlength, sizeof(char*), bcompare);
        if (match != NULL)
        {
            printf("%d. The matched password and hash is: %s\n" , count, *match);
            
        }
        count ++;
    }
    
    for (int i = 0; dict[i]; i++)
    {
        free(dict[i]);
    }
   
    free(dict);
    free(hashes[0]);
    free(hashes);
}
